require 'timeout'
class ImageGuessesController < ResourcesController
  
  #
  #
  def new
    @parent = (params[:parent_type].constantize).find params[:parent_id]

    begin
      status = Timeout::timeout(10) {
        ImageGuess.get_image( @parent )    
        render action: 'show'      
      }      
    rescue Exception => e
      # siltently do nothing 
      render nothing: true
    end     
  end
  
  #
  #
  def update
    @parent = resource.guessable
    
    if params[:accepted] == 'true'
      resource.update_attribute( :accepted, 1 )
      @parent.image = resource.image
      @parent.save!
    else
      resource.accepted = -1
      resource.image = nil
      resource.save!
      ImageGuess.get_image @parent
    end
    
    render action: 'show'
  end
end
