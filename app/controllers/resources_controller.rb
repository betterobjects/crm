class ResourcesController < ApplicationController
  inherit_resources

  before_action :permit_params
  
  protected 
    
  #
  #
  def permit_params
    params.permit!
  end
  
end