class OpportunitiesController < ResourcesController
  
  belongs_to :organisation, optional: true
  
  before_action :redirect_with_parent, only: :show
  
  #
  #
  def index
    
    unless params[:id].blank?
      opportunitiy = Opportunity.find params[:id] 
      redirect_to [opportunitiy.organisation, opportunitiy]
    end
    
    @info = {}
    
    opportunities = end_of_association_chain.includes([:opportunity_status, :opportunity_typ, :organisation] )
    
    opportunities.where! ['opportunities.name like ?', "%#{params[:name]}%"] unless params[:name].blank?
    opportunities.where! ['organisations.name like ?', "%#{params[:organisation]}%"] unless params[:organisation].blank?    
    opportunities.where! ['opportunities.user_id = ?', params[:assigned]] unless params[:assigned].blank?    
    
    params[:status] ||= '_open'
    
    closed = false
    
    case params[:status]
      when "_open"
        opportunities.where!( 'opportunity_statuses.closed = 0' )
      when "_closed"
        opportunities.where!( 'opportunity_statuses.closed = 1' )
        closed = true        
      else
        opportunities.where!( ['opportunity_status_id = ?', params[:status]] )
        closed = OpportunityStatus.find( params[:status] ).closed?        
    end    
      
    @info[:max] = closed ? opportunities.maximum('amount * (probability/100.0)') : Opportunity.max_open unless @info[:max]
    
    set_info( opportunities ) unless closed
    
    @opportunities = opportunities.order('(amount * probability) DESC').page params[:page]    
  
  end
  
  
  #
  #
  def update
    update! { [resource.organisation, resource]}
  end
  
  protected
  
  #
  #
  def set_info( opportunities )
    
    return if (count = opportunities.size) <= 1

    @info[:count] = count
    
    return if @info[:count] <= 1
    
    @info[:open] = opportunities.open.sum(&:expected)
    @info[:status] = {}
    opportunities.group_by(&:opportunity_status_id).each do |key, opportunities| 
      status = OpportunityStatus.find key
      @info[:status][status] = [opportunities.sum(&:expected)]
      @info[:status][status] << @info[:status][status].first / @info[:open] 
    end

    @info[:lead_source] = {}
    opportunities.group_by(&:lead_source_id).each do |key, opportunities| 
      lead_source = LeadSource.find key
      @info[:lead_source][lead_source] = [opportunities.sum(&:expected)]
      @info[:lead_source][lead_source] << @info[:lead_source][lead_source].first / @info[:open] 
    end
    
    @info[:opportunity_typ] = {}
    opportunities.group_by(&:opportunity_typ_id).each do |key, opportunities| 
      typ = OpportunityTyp.find key
      @info[:opportunity_typ][typ] = [opportunities.sum(&:expected)]
      @info[:opportunity_typ][typ] << @info[:opportunity_typ][typ].first / @info[:open] 
    end
    
  end
  
  #
  #
  def redirect_with_parent
    unless parent
      redirect_to [resource.organisation, resource]
    end
  end
  
end
