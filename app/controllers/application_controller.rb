class ApplicationController < ActionController::Base
  
  include ApplicationHelper
  
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :sso_authenticate!  
  layout :get_layout
  
  opinio_identifier do |params|
    next Contact.find(params[:contact_id]) if params[:contact_id]
    next Organisation.find(params[:organisation_id]) if params[:organisation_id]
  end
  
  protected
  
  #
  #
  def get_layout
    if self.class.to_s.start_with? "Admin::"
      'active_admin'
    else
      'application'
    end    
  end
  
  #
  #
  def sso_authenticate!
    return if current_user

    encrypted = cookies[:sso]
    if encrypted 
      cipher = OpenSSL::Cipher::Cipher.new("aes-256-cbc")
      cipher.decrypt
      cipher.key = Digest::SHA1.hexdigest("kmf-ohiwa-crm-2013-83jdhHU7Grt34")
      decrypted = cipher.update(encrypted)
      decrypted << cipher.final
    
      user = User.where( email: decrypted ).first
    
      if user
        puts "\n\n\n----------------signed in: #{user.name}-----------------------\n\n\n"
        sign_in 'user', user
        redirect_to root_path
        return
      end
    end
    
    redirect_to Rails.configuration.sso_sign_in_url    
  end
end
