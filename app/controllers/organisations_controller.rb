class OrganisationsController < ResourcesController
  
  #
  #
  def index
    @organisations = Organisation.all
    
    @organisations.where! ['organisations.website like ?', "%#{params[:website]}%"] unless params[:website].blank?
    @organisations.where! ['organisations.city like ?', "%#{params[:city]}%"] unless params[:city].blank?
    @organisations.where! ['organisations.name like ? or organisations.official_name like ?', "%#{params[:name]}%", "%#{params[:name]}%"] unless params[:name].blank?
    @organisations.where! ['org_type_id = ?', params[:org_type]] unless params[:org_type].blank?
    @organisations.where! ['id = ?', params[:id]] unless params[:id].blank?
    
    @organisations = @organisations.order(:name).page params[:page]
  end
  
  #
  #
  def contacts_list
    resource
  end

  protected
  
end
