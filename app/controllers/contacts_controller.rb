class ContactsController < ResourcesController
  
  belongs_to :organisation, optional: true  
  respond_to :html, :json, :vcf

  custom_actions :resource => :vcard
  
  #
  #
  def index
    @contacts = end_of_association_chain
    
    @contacts = @contacts.order([:firstname, :lastname]).page params[:page]
  end
  
end
