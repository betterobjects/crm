class AuditsController < ResourcesController
  
  belongs_to :organisation, optional: true
  belongs_to :contact, optional: true
  
end
