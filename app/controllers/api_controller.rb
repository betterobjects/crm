class ApiController < ApplicationController
  
  
  #
  #
  def lookup
    params[:term] ||= ''
    
    terms = params[:term].split

    qualifier = terms.shift.downcase if terms.first and terms.first.end_with? ':' 
    
    if( qualifier.nil? or qualifier.start_with? 'o' )
      organisations = Organisation.all
    
      terms.each do |term|
        organisations.where!( ['organisations.name like ?', "%#{term}%"])
      end
    
      organisations.limit! 6
    else
      organisations = []
    end
    
    if qualifier.nil? or qualifier.start_with? 'c'
      contacts = Contact.all
    
      terms.each do |term|
        contacts.where!( ["(concat(contacts.firstname, ' ', contacts.lastname) like ?)", "%#{term}%"])
      end
    
      contacts.limit! 6
    else
      contacts = []
    end    
    
    @results = organisations + contacts    
    @results.sort!{|r1, r2| r1.name <=> r2.name }
    
    @more = @results.size > 5
    @results = @results.first(5)    
  end
  
  #
  #
  def tags
    json = ActsAsTaggableOn::Tag.all.map do |tag|
      { "id" => "#{tag.id}", "label" => tag.name, "value" => tag.name }
    end.to_json
    puts json
    render text: json
  end
  
end