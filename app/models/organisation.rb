class Organisation < ActiveRecord::Base
  
  audited
  attr_protected
  opinio_subjectum
  acts_as_taggable
  
  belongs_to :industry
  belongs_to :org_type
  has_one :image_guess, -> { where('accepted <> -1') }, :as => :guessable
  has_many :contacts
  has_many :reminder
  has_many :opportunities, -> { order('(amount * probability) DESC') }
  has_attached_file :image, :styles => { :medium => "200x200>", :thumb => "75x75>" }, :default_url => "/images/:style/missing_logo.png"  

  attr_accessor :delete_image, :reset_image_guess
  before_validation { image.clear if delete_image == '1'; ImageGuess.reset( self ) }
   
  validates_presence_of :name, :org_type_id
   
  #
  #
  def to_param
    "#{id}-#{name.parameterize}"
  end
  
  #
  #
  def all_comments
    Comment.where( ["(commentable_type = 'Organisation' and commentable_id = ?) or (commentable_type = 'Contact' and commentable_id in (?))",
      self.id, self.contacts.map(&:id)] ).order( :created_at => :desc ) 
  end
  
  #
  #
  def current_contacts
    self.contacts.where( ex_organisation: false )
  end
  
  #
  #
  def image_search_urls
    term = "logo \"#{(not self.official_name.blank?) ? self.official_name : self.name}\""
    ["http://ajax.googleapis.com/ajax/services/search/images?imgsz=medium&v=1.0&q=#{CGI.escape(term)}"]
  end
      
end
