class ImageGuess < ActiveRecord::Base
  
  belongs_to :guessable, polymorphic: true
  has_attached_file :image, :styles => { :medium => "200x200>", :thumb => "75x75>" }
  
  
  #
  #
  def self.reset( guessable )
    ImageGuess.where( ["guessable_type = ? and guessable_id = ?", guessable.class.to_s, guessable.id] ).destroy_all
  end
  
  #
  #
  def self.get_image( guessable )    
    existing_urls = ImageGuess.where( ["guessable_type = ? and guessable_id = ?", guessable.class.to_s, guessable.id] ).map(&:url)
    
    search_images( guessable ).each do |url|
      puts "image_url: #{url}"
      next if url.blank? 
      next if existing_urls.include? url
            
      image_guess = ImageGuess.create( guessable: guessable, url: url )
      begin
        image_guess.image = URI.parse( url )
      rescue Exception => e
        Rails.logger.error( "Exception: #{e}" )
        next
      end
      image_guess.save!
      return      
    end
    
  end  
  
  protected
  
  #
  #
  def self.search_images( guessable  )  
    urls = []
    guessable.image_search_urls.each do |url|
      begin
        json_results = open(url) {|f| f.read };
        next if json_results.blank?
        results = JSON.parse(json_results)
        next unless results['responseData']
        urls += results['responseData']['results'].map{ |result| result['unescapedUrl'] }        
      rescue Exception => e
        Rails.logger.error e        
      end
    end
    urls
  end
  
end
