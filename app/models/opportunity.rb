class Opportunity < ActiveRecord::Base
  
  audited
  attr_protected
  opinio_subjectum
  acts_as_taggable
  
  belongs_to :organisation
  belongs_to :contact
  belongs_to :user
  belongs_to :opportunity_status
  belongs_to :opportunity_typ
  belongs_to :lead_source
  has_many :reminders, as: :reminderable
  
  scope :open, -> { includes(:opportunity_status).where( 'opportunity_statuses.closed' => false ) }
  
  validates_presence_of :organisation_id, :opportunity_status_id, :opportunity_typ_id, :lead_source_id, :name, :amount, :probability, :user_id
  validates_numericality_of :amount, :probability, only_integer: true, greater_than: 0
  
  #
  #
  def self.max_open
    Opportunity.includes(:opportunity_status).where( 'opportunity_statuses.closed = 0' ).maximum('amount * (probability/100.0)')
  end
  
  #
  #
  def self.open_sum
    self.open.sum(&:expected)
  end
  
  #
  #
  def expected
    amount * (probability / 100.0)
  end
  
end
