class Contact < ActiveRecord::Base
    
  audited
  attr_protected
  opinio_subjectum
  acts_as_taggable

  belongs_to :organisation
  belongs_to :salutation
  
  has_many :reminders, as: :reminderable
  has_one :image_guess, -> { where('accepted <> -1') }, :as => :guessable
  has_attached_file :image, :styles => { :medium => "250x250>", :thumb => "75x75>" }, :default_url => lambda{|image| "/images/:style/missing_#{image.instance.gender}.png"  }
  validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]


  attr_accessor :delete_image
  before_validation { image.clear if delete_image == '1'; ImageGuess.reset( self ) }
  
  validates_presence_of :lastname, :firstname

  #
  #
  def to_param
    "#{id}-#{name.parameterize}"
  end
  
  #
  #
  def name
    "#{firstname} #{lastname}"
  end
  
  #
  #
  def gender
    return "unknown" unless salutation
    return "male" if salutation.gender == 'm'
    return "female" if salutation.gender == 'f'
    return "unknown"
  end

  #
  #
  def all_comments
    organisation_id = self.organisation ? self.organisation.id : -1
    Comment.where( ["(commentable_type = 'Organisation' and commentable_id = ?) or (commentable_type = 'Contact' and commentable_id = ?)",
      organisation_id, self.id] ).order( :created_at => :desc ) 
  end
  
  #
  #
  def department_position
    [department.blank? ? nil : department, position.blank? ? nil : position].compact
  end

  #
  #
  def vcard
    VcardBuilder.make do |maker|
      maker.add_name do |name|
        name.prefix = self.salutation.to_s unless self.salutation.blank?
        name.given = self.firstname.to_s unless self.firstname.blank?
        name.family = self.lastname.to_s unless self.lastname.blank?
      end

      maker.add_addr do |addr|
        addr.preferred = true
        addr.location = 'work'
        street = [self.address1, self.address2].compact.join("\n")
        addr.street = street unless street.blank?
        addr.locality = "#{self.zipcode} #{self.city}" unless self.city.blank? and self.zipcode.blank?
        c = ISO3166::Country[self.country]
        addr.country = c.name if c
      end

      maker.birthday = self.birthdate if self.birthdate

      maker.add_photo do |photo|
        photo.image = File.open(self.image.path(:thumb)).read 
        photo.type = 'jpeg'
      end if self.image?

      maker.add_tel( self.phone_work ) { |t| t.location = 'work'; t.preferred = true } unless self.phone_work.blank?
      maker.add_tel( self.phone_mobile ) { |t| t.location = 'mobile' } unless self.phone_mobile.blank?
      
      maker.add_tel( self.fax ) do |tel|
        tel.location = 'work'
        tel.capability = 'fax'
      end unless self.fax.blank?

      maker.add_email(self.email) { |e| e.location = 'work' } unless self.email.blank?
    end
  end
  

  #
  #
  def image_search_urls
    ['xing.com', 'linkedin.com'].map do |site|
      "http://ajax.googleapis.com/ajax/services/search/images?imgsz=medium&v=1.0&q=#{CGI.escape(name)}" +
        "&imgtype=face&as_sitesearch=#{site}"
    end    
  end

end
