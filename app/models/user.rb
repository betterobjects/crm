class User < ActiveRecord::Base

  attr_protected
  
  self.primary_key = 'id'
  
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
         
  scope :assignable, -> { where( active: true ).where( status: ['pm', 'td', 'vice-admin', 'admin']).order( 'users.firstname, users.lastname') }
  
  #
  #
  def name
    "#{firstname} #{lastname}"
  end
  
  def admin?
    self.status == "admin"
  end
end
