module ApplicationHelper
  
  #
  #
  def navigation_item( key, resource, active )
    path = resource.is_a?( String ) ? resource : polymorphic_path( resource )
    content_tag( :li, link_to( key, path ), class: active ? 'active' : nil )
  end
  
  #
  #
  def list_group_item value, label = nil
    return nil if value.nil? or value.to_s.blank?
    value = label ? content_tag( :label, label ) + ' ' + value : value    
		content_tag :li, value, class: "list-group-item" 
  end
  
  #
  #
  def lna date
    date.blank? ? '' :  I18n.localize( date, format: "%d.%m.%Y" )
  end
  
  #
  #
  def nf( number, precision = 0 )
    return "" if number.blank?
    number_with_precision( number, delimiter: ".", separator: ",", precision: precision )
  end
  
  #
  #
  def can_destroy_opinio? comment
    current_user.admin? or comment_owner == current_user
  end
  
  #
  #
  def country_select_options
    preferred = [["Germany", "DE"], ["Switzerland", "CH"], ["United Kingdom", "GB"], ["France", "FR"], ["-------------", nil] ]
    preferred + (ISO3166::Country.all - preferred)
  end

  #
  #
  def languages
    @@languages ||= {'de' => 'deutsch', 'en' => 'english', 'fr' => 'français', 'es' => 'español', 'it' => 'italiano'}
  end
  
  #
  #
  def language( lang )
    languages[lang] || "Unknkown"
  end

  #
  #
  def opportunity_status_options
    options = [["Open (all)", "_open"], ["Closed (all)", "_closed"],["----------------------", "_none"]]
    options += OpportunityStatus.order( :name ).map{|o| [o.name, o.id ] }
    options
  end
      
  #
  #
  def space( count = 1 )
    ("&nbsp;" * count).html_safe 
  end
    
end
