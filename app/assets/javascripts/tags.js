$(function(){	
	$(document).on("ready page:change", function(){ enable_tags() } );
});

/**
 *
 */
function enable_tags() {
	$('.tags').each( function(){
		if( $(this).data('tag-enabled') == 'true' ) return;
		$(this).data('tag-enabled', 'true' );
		$(this).tagsInput({ 
			'autocomplete_url': '/api/tags'
		});		
	});
}
