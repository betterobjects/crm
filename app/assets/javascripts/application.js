// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery-ui
//= require jquery_ujs
//= require turbolinks
//= require jquery-ui/datepicker
//= require_tree .

$(function(){	
	$(document).on("ready page:change", function(){ onPageChange() } );

	$(document.body).on( 'mouseenter','.show-on-hover', function(){
		$( $(this).attr('data-show-on-hover') ).removeClass("hide");
	});

	$(document.body).on( 'mouseleave','.show-on-hover', function(){
		$( $(this).attr('data-show-on-hover') ).addClass("hide");
	});

	$(document.body).on( 'click','.wice-grid td', function(){
		var link = $(this).find('a');
		if( link ) Turbolinks.visit( link.attr('href') );
	});

});

/**
 *
 */
function onPageChange(){
	$('.date-mask').mask("99.99.9999",{placeholder:"-"});

	$('.date-picker').datepicker({ dateFormat: 'dd.mm.yy'} );
	
	$(document.body).on( 'change', '.auto-submit', function(){
		var form = $(this).closest('form');
		if( form ) form.submit();
	});	
}
