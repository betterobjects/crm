$(document).on('click', '.show-organisation-contacts', function(){
	showOrganisationContacts( $(this).data('organisation-id'), $(this).data('show') );
});

$(document).on('click', 'li.organisation-contact', function(){
	href = $(this).find('a').attr('href');
	if( ! href ) return;
	Turbolinks.visit( href );
});


function showOrganisationContacts( id, show ){
	$('#organisation-contacts').load( '/organisations/' + id + '/contacts_list?show=' + show );
}