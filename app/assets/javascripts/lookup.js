$(function(){	
	$(document).on("ready page:change", function(){ enable_lookup() } );
});

/**
 *
 */
function enable_lookup() {
	$( "#lookup" ).submit( function(){
		return false;
	});
	
	$( "#lookup" ).autocomplete({
		minLength: 2,		
	  source: '/lookup',
	  focus: function( event, ui ) {
	  	// $( "#lookup" ).val( ui.item.name );
	    return false;
	   },
	   select: function( event, ui ) {
			 Turbolinks.visit(ui.item.url );
	    return false;
	   }
	})
	.data( "ui-autocomplete" )
	._renderItem = function( ul, item ) {
		return render_lookup_item( item ).appendTo( ul );
	};;
}

/**
 *
 */
function render_lookup_item( item ){
	var li = $( "<li>" ).addClass('text-center');
	var a = $('<a>').appendTo( li );
	a.append( item.html );
	return li;
}
