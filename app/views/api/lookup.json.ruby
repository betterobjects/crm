@results.map! do |result|
  {
    type: result.class.to_s,
    id: result.id,
    name: result.name,
    html: render( partial: result.class.to_s.downcase, object: result ),
    url: polymorphic_path( result )
  }
end

@results << { html: link_to( "More ...", contacts_path ,class: 'text-muted text-center' ) } if @more

@results.to_json