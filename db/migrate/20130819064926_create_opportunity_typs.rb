class CreateOpportunityTyps < ActiveRecord::Migration
  
  def change
    create_table :opportunity_typs do |t|
      t.string :key
      t.string :name
      t.string :color
      
      t.timestamps
    end
    
    
    OpportunityTyp.reset_column_information
    
    types = {
      "New Business" => ["new_business", "FF6666"],
      "Existing Business" => ["existing_business", "006666"],
      "Reactivated Business" => ["reactivated_business", "9966CC"]
    }
    
    types.each do |name, value|
      OpportunityTyp.create! key: value[0], name: name, color: value[1]
    end
  end
end
