class CreateReminders < ActiveRecord::Migration
  def change
    create_table :reminders do |t|
      t.string :reminderable_type
      t.integer :reminderable_id
      t.string :typ
      t.date :date
      t.text :note
      t.references :user, index: true
      t.boolean :done

      t.timestamps
    end
  end
end
