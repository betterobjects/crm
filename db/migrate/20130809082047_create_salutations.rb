class CreateSalutations < ActiveRecord::Migration
  def change
    create_table :salutations do |t|
      t.string :name
      t.string :gender

      t.timestamps
    end
  end
end
