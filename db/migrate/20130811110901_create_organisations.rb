

class CreateOrganisations < ActiveRecord::Migration
  
  class SugarAccount < ActiveRecord::Base
  end
    
  def change
    
    ActiveRecord::Base.connection().execute("drop view if EXISTS sugar_accounts")
    ActiveRecord::Base.connection().execute("CREATE VIEW `sugar_accounts` AS select * from `sugarcrm`.`accounts` a where a.deleted = 0")
    
    create_table :organisations do |t|
      t.boolean   :active,        default: true
      t.string    :name
      t.string    :official_name
      t.text      :description
      t.references :org_type
      t.references :industry
      t.string    :phone
      t.string    :fax
      t.string    :email
      t.string    :website
      t.string    :facebook
      t.string    :address1
      t.string    :address2
      t.string    :city
      t.string    :zipcode
      t.string    :country

      t.timestamps
    end
    
    add_index :organisations, :active
    add_index :organisations, :name
    add_index :organisations, :org_type_id
    add_index :organisations, :industry_id
    add_index :organisations, :city
    
    Organisation.reset_column_information
    
    # weird translation table for the stuff in old sugar
    countries = {
      "Germany" => "DE",
      "Deutschland" => "DE",
      "Switzerland" => "CH",
      "Schweiz" => "CH",
      "Austria" => "AT",
      "England" => "GB",
      "USA" => "US",  
      "Hamburg" => "DE",
      "United Kingdom" => "GB",
      "France" => "FR",
      "Niederlande" => "NL",
      "Germnay" => "DE",
      "Germanz" => "DE",
      "The Netherlands" => "NL",
      "Czech Republic" => "CZ",
      "Köln" => "DE",
      "Ireland" => "IE",
      "CH" => "CH",
      "Denmark" => "DK",
      "Norway" => "NO",
      "Poland" => "PL",
      "Österreich" => "AT",
      "Belgium" => "BE",
      "UK" => "GB",
      "Luxembourg" => "LU",
      "Brazil" => "BR",
      "Italia" => "IT",
      "Canada" => "CA",
      "D-" => "DE"
    }
    
    Industry.reset_column_information
    r = ActiveRecord::Base.connection().execute("select distinct industry from sugar_accounts where industry is not null and industry <> ''")
    
    r.each do |i|
      Industry.create! key: i.first.parameterize.underscore, name: i.first
    end
    

    OrgType.reset_column_information
    r = ActiveRecord::Base.connection().execute("select distinct account_type from sugar_accounts where account_type is not null and account_type <> ''")
    
    r.each do |i|
      OrgType.create! key: i.first.parameterize.underscore, name: i.first
    end
        
    SugarAccount.where( "date_entered > '2012-06-14 00:00:00'").each do |sa|
      webiste = nil
      website = sa.website unless sa.website.strip == "http://" 
  
      orgType = OrgType.where( name: sa.account_type ).first
      orgType = OrgType.where( name: "Other" ).first unless orgType
    
      industry = Industry.where( name: sa.industry ).first
      
      org = Organisation.create! name: sa.name, description: sa.description, org_type: orgType, industry: industry, 
        phone: sa.phone_office, fax: sa.phone_fax, website: website, address1: sa.billing_address_street, address2: sa.billing_address_state, 
        zipcode: sa.billing_address_postalcode, city: sa.billing_address_city, country: countries[sa.billing_address_country]
        
      audit = org.audits.first
      audit.created_at = sa.date_entered
      audit.username = "SugarCrm"
      audit.save!
    end
    
    ActiveRecord::Base.connection().execute("drop view sugar_accounts")
  end
end













