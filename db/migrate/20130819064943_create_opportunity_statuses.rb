class CreateOpportunityStatuses < ActiveRecord::Migration
  def change
    create_table :opportunity_statuses do |t|
      t.string :key
      t.string :name
      t.string :color
      t.boolean :closed, default: false

      t.timestamps
    end
    
    OpportunityStatus.reset_column_information
    
    statuses = {
      "Idea" => ["idea", false, "FF6666"],
      "Prospecting" => ["prospecting", false, "006666"],
      "Quote/Negotiation" => ["quote_negotiation", false, "9966CC" ],
      "On-hold" => ["onhold", true, "999900"],
      "Closed Lost" => ["closed_lost", true, "999900"],
      "Closed Won" => ["closed_won", true, "999900"],
      "Closed Partly Won" => ["closed_partly_won", true, "999900"]
    }
  
    
    statuses.each do |name, value|
      OpportunityStatus.create! key: value[0], name: name, closed: value[1], color: value[2]
    end
      
  end
end
