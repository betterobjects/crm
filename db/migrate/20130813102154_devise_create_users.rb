class DeviseCreateUsers < ActiveRecord::Migration
  def change
    ActiveRecord::Base.connection().execute("drop view if EXISTS users")
    ActiveRecord::Base.connection().execute("CREATE VIEW `users` AS select * from `ohiwa-devel`.`employees`")    
  end
end
