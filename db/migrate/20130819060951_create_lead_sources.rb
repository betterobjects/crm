class CreateLeadSources < ActiveRecord::Migration
  def change
    create_table :lead_sources do |t|
      t.string :key
      t.string :name
      t.string :color

      t.timestamps
    end
    
    add_index :lead_sources, :key    
    
    LeadSource.reset_column_information
    
    lead_sources = {
      "Cold call" => ["cold_call", "9966CC"],
      "Word of mouth" => ["word_of_mouth", "00666"],
      "Other" => ["other", "9966CC"],
      "Existing Client" => ["existing_client", "999900"],
      "Employee" => ["employee", "99CCCC"],
      "UDG Pitch" => ["udg_pitch", "FF9966"],
      "Existing UDG Client" => ["udg_existing", "FFCC99"],
      "Former Client" => ["former_client", "FFFF99"]
    }
    
    lead_sources.each do |name, value|
      LeadSource.create! key: value[0], name: name, color: value[1]
    end
    
  end
end
