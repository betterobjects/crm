class CreateOpportunities < ActiveRecord::Migration

  class SugarOpportunity < ActiveRecord::Base; end

  def change
    create_table :opportunities do |t|
      t.references :organisation, index: true
      t.references :contact, index: true
      t.references :user, index: true
      t.references :opportunity_status, index: true
      t.references :opportunity_typ, index: true
      t.references :lead_source, index: true
      t.string :name
      t.text :description
      t.text :next_steps
      t.date :next_date
      t.integer :amount
      t.integer :probability

      t.timestamps
    end
    
    types = {
      "New Business" => OpportunityTyp.where( key: "new_business" ).first,
      "Existing Business" => OpportunityTyp.where( key: "existing_business" ).first
    }
    
    lead_sources = {
      "Word of mouth" => LeadSource.where( key: "word_of_mouth" ).first,
      "Other" => LeadSource.where( key: "other" ).first,
      "Existing Customer" => LeadSource.where( key: "existing_client" ).first,
      "Employee" => LeadSource.where( key: "employee" ).first,
      "udg_pitch" => LeadSource.where( key: "udg_pitch" ).first,
      "udg_existing" => LeadSource.where( key: "udg_existing" ).first,
      "former_customer" => LeadSource.where( key: "former_client" ).first
    }
        
    statuses = {
      "Closed Won" => OpportunityStatus.where( key: "closed_won" ).first,
      "Prospecting" => OpportunityStatus.where( key: "prospecting" ).first,
      "Closed Lost" => OpportunityStatus.where( key: "closed_lost" ).first,
      "Proposal/Price Quote" => OpportunityStatus.where( key: "quote_negotiation" ).first,
      "Negotiation/Review" => OpportunityStatus.where( key: "quote_negotiation" ).first,
      "Qualification" => OpportunityStatus.where( key: "quote_negotiation" ).first,
      "Value Proposition" => OpportunityStatus.where( key: "prospecting" ).first,
      "Perception Analysis" => OpportunityStatus.where( key: "prospecting" ).first,
      "Needs Analysis"  => OpportunityStatus.where( key: "prospecting" ).first
    }
    
    ActiveRecord::Base.connection().execute("drop view if EXISTS sugar_opportunities")
    
    view = <<EOL
      CREATE VIEW `sugar_opportunities` AS
      select a.name as 'account_name', c.first_name, c.last_name, u.`first_name` as 'user_first', u.`last_name` as 'user_last', o.*
      from `sugarcrm`.opportunities o
      left join `sugarcrm`.`accounts_opportunities` ao on ao.`opportunity_id` = o.`id`
      left join `sugarcrm`.`accounts` a on ao.`account_id` = a.id
      left join `sugarcrm`.`opportunities_contacts` oc on oc.`opportunity_id` = o.id
      left join `sugarcrm`.contacts c on oc.`contact_id` = c.id
      left join `sugarcrm`.`users` u on o.`assigned_user_id` = u.`id`
      where o.deleted = 0
      and ao.`deleted` = 0 
EOL
    ActiveRecord::Base.connection().execute( view )

    processed = []

    SugarOpportunity.where( "date_entered > '2012-06-14 00:00:00'").each do |so|
      
      next if processed.include? so.id
      processed << so.id
      
      organisation = Organisation.where( name: so.account_name ).first
      next unless organisation
      contact = Contact.where( lastname: so.last_name, firstname: so.first_name ).first
      
      typ = types[so.opportunity_type]
      typ = OpportunityTyp.where( key: "new_business" ).first unless typ
      
      lead_source = lead_sources[so.lead_source]
      lead_source = LeadSource.where( key: 'other' ).first unless lead_source
      
      user = User.where( lastname: so.user_last, firstname: so.user_first ).first
      
      next unless user
      
      next if so.amount.to_i == 0 or so.probability.to_i == 0 
        
      opp = Opportunity.create! name: so.name, organisation: organisation, contact: contact, description: so.description, 
                                next_steps: so.next_step, next_date: so.date_closed, opportunity_typ: typ,
                                lead_source: lead_source, opportunity_status: statuses[so.sales_stage], 
                                amount: so.amount.to_i, probability: so.probability.to_i, user: user
      
      audit = opp.audits.first
      audit.created_at = so.date_entered
      audit.username = "SugarCrm"
      audit.save!      
    end

    ActiveRecord::Base.connection().execute("drop view if EXISTS sugar_opportunities")        
  end
end
