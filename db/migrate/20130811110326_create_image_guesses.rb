class CreateImageGuesses < ActiveRecord::Migration
  def change
    create_table :image_guesses do |t|
      t.string :guessable_type
      t.integer :guessable_id
      t.string :url
      t.integer :position
      t.integer :accepted, default: 0

      t.timestamps
    end
    
    add_attachment :image_guesses, :image
  end
end
