
=begin
CREATE VIEW `contacts-devel`.`tmp_contacts` AS 
	select NULL, true, `last_name`, `first_name`, salutation, title, department, `phone_work`, `phone_mobile`, `phone_other`, phone_home, 
  `phone_fax`, `primary_address_street`, `primary_address_state`, `primary_address_postalcode`, `primary_address_city`, 
  `primary_address_country`, `birthdate`
	from `sugarcrm`.`contacts` c;
=end

class CreateContacts < ActiveRecord::Migration
  
  class SugarContact < ActiveRecord::Base
  end
  
  def change
    
    ActiveRecord::Base.connection().execute("drop view if EXISTS sugar_contacts")
    
    create_view = <<EOL
      CREATE VIEW `sugar_contacts` AS
      select a.id as account_id, a.name as account_name, c.`last_name`, c.`first_name`, ea.`email_address`,
      		c.salutation, title, department, `phone_work`, `phone_mobile`, `phone_other`, phone_home, c.`phone_fax`, 
            	c.`primary_address_street`, c.`primary_address_state`, c.`primary_address_postalcode`, c.`primary_address_city`, c.`primary_address_country`, `birthdate`, 
              c.description, c.date_entered, cc.`language_c`, cc.`skype_c`, cc.`position_c`
      from `sugarcrm`.`contacts` c
      join `sugarcrm`.`accounts_contacts` ac on ac.`contact_id` = c.id
      join `sugarcrm`.`accounts` a on a.id = ac.`account_id`
      join `sugarcrm`.`email_addr_bean_rel` eabr on eabr.`bean_id` = c.id
      join `sugarcrm`.`email_addresses` ea on ea.id = eabr.`email_address_id`
      join `sugarcrm`.`contacts_cstm` cc on c.id = cc.`id_c`
      where ac.`deleted` = 0 and c.deleted = 0 and a.deleted = 0 
      and ea.`deleted` = 0 and ea.`invalid_email` = 0 and eabr.`primary_address` = 1
EOL

    ActiveRecord::Base.connection().execute(create_view)
    
    create_table :contacts do |t|
      t.boolean     :active,        default: true
      t.string      :lastname
      t.string      :firstname
      t.references  :salutation
      t.string      :title
      t.string      :position
      t.references  :organisation
      t.boolean     :ex_organisation, default: false
      t.string      :department
      t.string      :email
      t.string      :phone_work
      t.string      :phone_mobile
      t.string      :phone_private
      t.string      :phone_other
      t.string      :fax
      t.string      :skype
      t.date        :birthdate
      t.string      :address1
      t.string      :address2
      t.string      :zipcode
      t.string      :city
      t.string      :country
      t.text        :description
      t.string      :language

      t.timestamps
    end
    
    add_index :contacts, :active
    add_index :contacts, :lastname
    add_index :contacts, :firstname
    add_index :contacts, :organisation_id
    add_index :contacts, :city
    
    Contact.reset_column_information
        
    puts "SugarContact.all.count: #{SugarContact.all.count}"
    
    countries = {
      "Germany" => "DE",
      "Deutschland" => "DE",
      "Switzerland" => "CH",
      "Schweiz" => "CH",
      "Austria" => "AT",
      "England" => "GB",
      "USA" => "US",  
      "Hamburg" => "DE",
      "United Kingdom" => "GB",
      "France" => "FR",
      "Niederlande" => "NL",
      "Germnay" => "DE",
      "Germanz" => "DE",
      "The Netherlands" => "NL",
      "Czech Republic" => "CZ",
      "Köln" => "DE",
      "Ireland" => "IE",
      "CH" => "CH",
      "Denmark" => "DK",
      "Norway" => "NO",
      "Poland" => "PL",
      "Österreich" => "AT",
      "Belgium" => "BE",
      "UK" => "GB",
      "Luxembourg" => "LU",
      "Brazil" => "BR",
      "Italia" => "IT",
      "Canada" => "CA",
      "D-" => "DE",
      "Italien" => "IT",
      "DENMARK" => "DK",
      "Vereinigtes Königreich" => "GB",
      "Italy" => "IT",
      "CZ" => "CZ",
      "Südafrika" => "ZA",
      "Polen" => "PL",
      "Belgien" => "BE"
    }
    
    languages = {
      'Englisch' => 'en',
      'Franzoesisch' => 'fr'
    }
    
    Salutation.reset_column_information
    r = ActiveRecord::Base.connection().execute("select distinct salutation from sugar_contacts where salutation is not null and salutation <> ''")
    
    r.each do |i|
      Salutation.create! name: i.first
    end    
    
    SugarContact.all.each do |sc|
      organisation = Organisation.where( name: sc.account_name ).first
      salutation = Salutation.where( name: sc.salutation ).first
      firstname = sc.first_name.blank? ? "n/a" : sc.first_name
      lastname = sc.last_name.blank? ? "n/a" : sc.last_name
      
      contact = Contact.create! organisation: organisation, lastname: sc.last_name, firstname: firstname, salutation: salutation, 
        title: sc.title, department: sc.department, phone_work: sc.phone_work, phone_mobile: sc.phone_mobile, phone_other: sc.phone_other,
        fax: sc.phone_fax, birthdate: sc.birthdate, address1: sc.primary_address_street, address2: sc.primary_address_state, 
        zipcode: sc.primary_address_postalcode, city: sc.primary_address_city, country: countries[sc.primary_address_country], 
        description: sc.description, email: sc.email_address, skype: sc.skype_c, position: sc.position_c, language: languages[sc.language_c]
        
      audit = contact.audits.first
      audit.created_at = sc.date_entered
      audit.username = "SugarCrm"
      audit.save!
        
    end
    
    ActiveRecord::Base.connection().execute("drop view sugar_contacts")
    
    # all audits back to original
    
  end
end
