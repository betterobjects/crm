class AddAttachmentLogoToOrganisations < ActiveRecord::Migration
  def self.up
    change_table :organisations do |t|
      t.attachment :image
    end
  end

  def self.down
    drop_attached_file :organisations, :image
  end
end
