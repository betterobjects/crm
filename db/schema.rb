# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160603235652) do

  create_table "active_admin_comments", force: true do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "audits", force: true do |t|
    t.integer  "auditable_id"
    t.string   "auditable_type"
    t.integer  "associated_id"
    t.string   "associated_type"
    t.integer  "user_id"
    t.string   "user_type"
    t.string   "username"
    t.string   "action"
    t.text     "audited_changes"
    t.integer  "version",         default: 0
    t.string   "comment"
    t.string   "remote_address"
    t.datetime "created_at"
    t.string   "request_uuid"
  end

  add_index "audits", ["associated_id", "associated_type"], name: "associated_index", using: :btree
  add_index "audits", ["auditable_id", "auditable_type"], name: "auditable_index", using: :btree
  add_index "audits", ["created_at"], name: "index_audits_on_created_at", using: :btree
  add_index "audits", ["request_uuid"], name: "index_audits_on_request_uuid", using: :btree
  add_index "audits", ["user_id", "user_type"], name: "user_index", using: :btree

  create_table "comments", force: true do |t|
    t.integer  "owner_id",         null: false
    t.integer  "commentable_id",   null: false
    t.string   "commentable_type", null: false
    t.text     "body",             null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "contacts", force: true do |t|
    t.boolean  "active",             default: true
    t.string   "lastname"
    t.string   "firstname"
    t.integer  "salutation_id"
    t.string   "title"
    t.string   "position"
    t.integer  "organisation_id"
    t.boolean  "ex_organisation",    default: false
    t.string   "department"
    t.string   "email"
    t.string   "phone_work"
    t.string   "phone_mobile"
    t.string   "phone_private"
    t.string   "phone_other"
    t.string   "fax"
    t.string   "skype"
    t.date     "birthdate"
    t.string   "address1"
    t.string   "address2"
    t.string   "zipcode"
    t.string   "city"
    t.string   "country"
    t.text     "description"
    t.string   "language"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  add_index "contacts", ["active"], name: "index_contacts_on_active", using: :btree
  add_index "contacts", ["city"], name: "index_contacts_on_city", using: :btree
  add_index "contacts", ["firstname"], name: "index_contacts_on_firstname", using: :btree
  add_index "contacts", ["lastname"], name: "index_contacts_on_lastname", using: :btree
  add_index "contacts", ["organisation_id"], name: "index_contacts_on_organisation_id", using: :btree

  create_table "image_guesses", force: true do |t|
    t.string   "guessable_type"
    t.integer  "guessable_id"
    t.string   "url"
    t.integer  "position"
    t.integer  "accepted",           default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  create_table "industries", force: true do |t|
    t.string   "key"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "lead_sources", force: true do |t|
    t.string   "key"
    t.string   "name"
    t.string   "color"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "lead_sources", ["key"], name: "index_lead_sources_on_key", using: :btree

  create_table "opportunities", force: true do |t|
    t.integer  "organisation_id"
    t.integer  "contact_id"
    t.integer  "user_id"
    t.integer  "opportunity_status_id"
    t.integer  "opportunity_typ_id"
    t.integer  "lead_source_id"
    t.string   "name"
    t.text     "description"
    t.text     "next_steps"
    t.date     "next_date"
    t.integer  "amount"
    t.integer  "probability"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "opportunities", ["contact_id"], name: "index_opportunities_on_contact_id", using: :btree
  add_index "opportunities", ["lead_source_id"], name: "index_opportunities_on_lead_source_id", using: :btree
  add_index "opportunities", ["opportunity_status_id"], name: "index_opportunities_on_opportunity_status_id", using: :btree
  add_index "opportunities", ["opportunity_typ_id"], name: "index_opportunities_on_opportunity_typ_id", using: :btree
  add_index "opportunities", ["organisation_id"], name: "index_opportunities_on_organisation_id", using: :btree
  add_index "opportunities", ["user_id"], name: "index_opportunities_on_user_id", using: :btree

  create_table "opportunity_statuses", force: true do |t|
    t.string   "key"
    t.string   "name"
    t.string   "color"
    t.boolean  "closed",     default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "opportunity_typs", force: true do |t|
    t.string   "key"
    t.string   "name"
    t.string   "color"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "org_types", force: true do |t|
    t.string   "key"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "organisations", force: true do |t|
    t.boolean  "active",             default: true
    t.string   "name"
    t.string   "official_name"
    t.text     "description"
    t.integer  "org_type_id"
    t.integer  "industry_id"
    t.string   "phone"
    t.string   "fax"
    t.string   "email"
    t.string   "website"
    t.string   "facebook"
    t.string   "address1"
    t.string   "address2"
    t.string   "city"
    t.string   "zipcode"
    t.string   "country"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  add_index "organisations", ["active"], name: "index_organisations_on_active", using: :btree
  add_index "organisations", ["city"], name: "index_organisations_on_city", using: :btree
  add_index "organisations", ["industry_id"], name: "index_organisations_on_industry_id", using: :btree
  add_index "organisations", ["name"], name: "index_organisations_on_name", using: :btree
  add_index "organisations", ["org_type_id"], name: "index_organisations_on_org_type_id", using: :btree

  create_table "reminders", force: true do |t|
    t.string   "reminderable_type"
    t.integer  "reminderable_id"
    t.string   "typ"
    t.date     "date"
    t.text     "note"
    t.integer  "user_id"
    t.boolean  "done"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "reminders", ["user_id"], name: "index_reminders_on_user_id", using: :btree

  create_table "salutations", force: true do |t|
    t.string   "name"
    t.string   "gender"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "taggings", force: true do |t|
    t.integer  "tag_id"
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.integer  "tagger_id"
    t.string   "tagger_type"
    t.string   "context",       limit: 128
    t.datetime "created_at"
  end

  add_index "taggings", ["tag_id"], name: "index_taggings_on_tag_id", using: :btree
  add_index "taggings", ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context", using: :btree

  create_table "tags", force: true do |t|
    t.string  "name"
    t.integer "taggings_count"
  end

  create_table "users", id: false, force: true do |t|
    t.integer  "id",                                 default: 0,       null: false
    t.boolean  "active",                             default: true
    t.string   "email",                              default: "",      null: false
    t.string   "lastname"
    t.string   "firstname"
    t.string   "gender",                 limit: 1
    t.date     "birthdate"
    t.string   "status"
    t.string   "abbreviation"
    t.integer  "jobtitle_id"
    t.date     "date_of_joining"
    t.date     "date_of_leaving"
    t.integer  "internalwage"
    t.integer  "redmine_id"
    t.string   "default_start",                      default: "09:00"
    t.string   "default_end",                        default: "18:00"
    t.integer  "default_break",                      default: 60
    t.string   "encrypted_password",     limit: 128, default: "",      null: false
    t.string   "reset_password_token"
    t.string   "remember_token"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                      default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                           null: false
    t.datetime "updated_at",                                           null: false
    t.integer  "booking_reminder_grace"
    t.integer  "booking_reminder_count"
    t.date     "last_booking_reminder"
    t.boolean  "planbookable"
    t.boolean  "freelancer"
    t.boolean  "atwork",                             default: true
    t.string   "phone"
    t.string   "im"
    t.string   "car"
    t.string   "cal_url"
    t.string   "booking_note"
    t.integer  "location_id"
    t.string   "mobile"
    t.string   "locale"
  end

end
