class VcardBuilder 
  
  #
  #
  def self.make &block
    card21 = Vcard::DirectoryInfo.create(
      [ Vcard::DirectoryInfo::Field.create('VERSION', '2.1') ], 'VCARD' )

    Vcard::Vcard::Maker.make2(card21) do |maker|
      yield maker
    end
    card21.to_s    
  end
end